#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# -----------------------------------------------------------------------------

ENABLE_DRAW=0

# cglobal打印标志
ENABLE_GENERIC_DEBUG=0

# chandler打印标志
ENABLE_HANDLE_DEBUG=0

# cvisiodraw打印标志
ENABLE_DRAW_DEBUG=0

# visioflow打印标志
ENABLE_VISIOD_DEBUG=0

# yacc打印标志
ENABLE_YACC_DEBUG=0		

# YTOOL打印标志
ENABLE_YTOOL_DEBUG=0

# 打印 yacc 中的堆栈调用信息
CURINFO=0

# 是否答应doxy yacc的解析
DEBUG_DOXYYACC=0
