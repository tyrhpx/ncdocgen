#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# -----------------------------------------------------------------------------
# condraw.py
#
# 

import re

from cglobal import *

def print_prefix(n, x):
    print FUN_LFMT%n, FUN_DELIM*x,

def print_blank():
    print(FUN_BLANK)

# 使用递归的方式来进行打印
#   x   x的偏移量
#   y   递归的深度
#   return 
def print_func_exp(exp, x=0, y=0):
    ttype = exp[TK_TYPE]
    if (
            ttype == 'DONENODE'
            or ttype == 'DONECODE'
            ):
        # 如果是最终节点，答应即可
        for node in (exp[TK_EXPR]):
            print_prefix(node[TK_START], x)
            print node[TK_EXPR] 
    elif (
            ttype == 'DONEBREAK'
            or ttype == 'DONECONTINUE'
            or ttype == 'DONEGOTO'
            or ttype == 'DONERETURN'
            ):
        op = ttype[4:len(ttype)].lower()
        # 如果是最终节点，答应即可
        for node in (exp[TK_EXPR]):
            print_prefix(node[TK_START], x)
            print node[TK_EXPR] 
        print_blank()
    elif (ttype == 'DONEIFELSE'):
        # 获取if的第一个节点
        node = exp[TK_EXPR][0]

        print_prefix(node[TK_START], x)
        print 'if(%s){  -->%d'%(node[TK_EXPR], node[TK_END])

        # 获取IF函数的节点
        print_func_exp(exp[TK_EXPR][1], x+1, y+1)

        # 获取ELSE
        print_prefix(node[TK_MID], x)
        print '}else(%s){  -->%d'%(node[TK_EXPR], node[TK_END])

        # 获取ELSE函数的节点
        print_func_exp(exp[TK_EXPR][2], x+1, y+1)

        print_prefix(node[TK_END], x)
        print '} <if(%d)'%(node[TK_START])

        print_blank()
    elif (ttype == 'DONEIF'
            or ttype == 'DONEWHILE'
            or ttype == 'DONECASE'
            or ttype == 'DONEDEFAULT'
            ):
        op = ttype[4:len(ttype)].lower()
        # 获取 第一个节点
        node = exp[TK_EXPR][0]

        print_prefix(node[TK_START], x)
        print op+'(%s){  -->%d'%(node[TK_EXPR], node[TK_END])

        # 获取函数的节点
        print_func_exp(exp[TK_EXPR][1], x+1, y+1)

        print_prefix(node[TK_END], x)
        print '} <%s(%d)'%(op, node[TK_START])
        print_blank()
    elif (ttype == 'DONEDO'):
        op = ttype[4:len(ttype)].lower()
        # 获取if的第一个节点
        node = exp[TK_EXPR][0]
        print_prefix(node[TK_START], x)
        print op+'{  -->%d'%(node[TK_END])

        # 获取函数的节点
        print_func_exp(exp[TK_EXPR][1], x+1, y+1)

        print_prefix(node[TK_END], x)
        print '}while <(%s)(-->%d)'%(node[TK_EXPR], node[TK_START])
        print_blank()
    elif (ttype == 'DONEFOR'):
        op = ttype[4:len(ttype)].lower()
        # 获取if的第一个节点
        node = exp[TK_EXPR][0]

        print_prefix(node[TK_START], x)
        print '..(%s)'%(node[TK_EXPR0])

        print_prefix(node[TK_START], x)
        print op+'(%s){  -->%d'%(node[TK_EXPR], node[TK_END])

        # 获取函数的节点
        print_func_exp(exp[TK_EXPR][1], x+1, y+1)

        print_prefix(node[TK_END], x)
        print '..(%s)'%(node[TK_EXPR2])
        print_prefix(node[TK_END], x)
        print '} <%s(%d)'%(op, node[TK_START])
        print_blank()
    elif (ttype == 'DONESWITCH'):
        op = ttype[4:len(ttype)].lower()
        # 获取 第一个节点
        node = exp[TK_EXPR][0]

        print_prefix(node[TK_START], x)
        print op+'(%s){  -->%d'%(node[TK_EXPR], node[TK_END])

        # 获取函数的节点
        # print_func_exp(exp[TK_EXPR][1], x+1, y+1)
        j=0
        for node2 in (exp[TK_EXPR][1][TK_EXPR]):
            # 遍历switch里面的分支判断
            if (node2[TK_TYPE] == 'DONECASE'
                    or node2[TK_TYPE] == 'DONEDEFAULT'
                    ):
                j+=1
            print_func_exp(node2, x+j, y+1)
        print_prefix(node[TK_END], x)
        print '} <%s(%d)'%(op, node[TK_START])
        print_blank()
    elif (ttype == 'NODE'):
        # 说明需要解析
        for node in exp[TK_EXPR]:
            print_func_exp(node, x, y)
        
    return ttype

import sys
sys.path.append('..')

def print_fun_header(name, header):
    for key in comment_seq_list:
        print CM_SEP
        print CM_FMTSTR%comment_translate_table[key],
        #print value                       

        lines = header[key].split('\n')
        print lines[0]
        try:
            for line in lines[1:]:
                print CM_FMTSTR%(' '),
                print line
        except:
            pass                          
    print CM_SEP

if __name__ == '__main__':
    tt = ['DONESWITCH', 11, 34, [
        ['SWITCH', 11, 34, 'sw?'], 
        ['NODE', 12, 34, 
         [['DONECASE', 14, 22, [['CASE', 14, 14, 'e2?'], 
                                ['NODE', 14, 22, [['DONENODE', 14, 14,
[['DONENODE', 14, 14, 'case e2']]], ['DONEIF', 16, 18, [['IF', 16, 18, 'dodo e2?'], ['DONENODE', 16, 18
, [['DONENODE', 17, 17, 'go ( ) ']]]]], ['DONENODE', 19, 22, [['DONENODE', 20, 20, '\xbf\xc9\xc4\xdc ']]]]]
                               ]], 
          ['DONEBREAK', 23, 23, [['BREAK', 23, 23, 'BRE break23']]], 
          ['DONECASE', 25, 25, [['CASE', 25, 25, 'e2?'], ['DONENODE', 25, 25, [['DONENODE', 25, 25, 'NOP']]]]], 
          ['DONECASE', 26, 30, [['CASE', 26, 26, 'e3?'], ['NODE', 26, 30, [['DONENODE', 26, 26, [['DONENODE', 26, 26, 'NOP']]], ['DONEIF', 27, 30, [['IF', 27, 30,
'dodo ( )?'], ['DONENODE', 28, 30, [['DONENODE', 29, 29, 'dodo1 ( ) ']]]]]]]]],
['DONEBREAK', 31, 31, [['BREAK', 31, 31, 'BRE break31']]], ['DONECASE', 32, 32,
[['CASE', 32, 32, 'tt?'], ['DONENODE', 32, 32, [['DONENODE', 32, 32, 'NOP']]]]]
, ['DONEBREAK', 33, 33, [['BREAK', 33, 33, 'BRE break33']]]]
        ]
    ]]

    tt2 = ['DONENODE', 1, 10, [
        ['DONENODE', 2, 3, 'gogo1'], 
        ['DONENODE', 5, 7, 'gogo2'], 
    ]]    
    
    tt3 = ['DONENODE', 1, 10, [
        ['DONENODE', 2, 3, 'gogo1'], 
        ['DONENODE', 2, 3, 'gogo2'], 
    ]]    
    #print print_func_exp(tt)
    print print_func_exp(tt2)
    print print_func_exp(tt3)


