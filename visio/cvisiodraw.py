#!/usr/bin/python
# encoding: GBK
#
# -----------------------------------------------------------------------------
# condraw.py
#
# 

import sys
sys.path.insert(0,"../common")

from debug import *

from cglobal import *
from visioflow import *

STR_YES='是'
STR_NO='否'

vdraw=visiodraw(get_full_name('_def.vsd'))

#
# 获取额外的属性，包括未关闭的节点等
#    
def get_extra_node(vexp):
    lenv = len(vexp)
    offs = VNODE_TYPE
    # 这是一个君子协定，如果数据不对齐，则退出
    if (lenv > offs):
        return vexp[offs:lenv]
    else:
        return []

# inloop 表示
def conn_extra_node(extra, cur, inloop=0):
    # 如果extra的长度>=2，则使用前一个节点和当前节点进行连接
    if ENABLE_VISIOD_DEBUG==1: print '====con ex', extra
    # 判断额外的类型
    for i in range(0,len(extra),2):
        if (extra[i+VNODE_PAIR_TYPE] == 'IF'
                or extra[i+VNODE_PAIR_TYPE] == 'WHILE'
                ):
            vdraw.conn_node(extra[i+VNODE_PAIR_NODE], cur, CONN_D2P, STR_NO)
        elif (extra[i+VNODE_PAIR_TYPE] == 'BREAK'
                ):
            if ENABLE_VISIOD_DEBUG==1: print '====con break', extra
            vdraw.conn_node(extra[i+VNODE_PAIR_NODE], cur, CONN_P2P, "BREAK")
        elif (extra[i+VNODE_PAIR_TYPE] == 'RETURN'):
            # 如果是RETURN，需要将之前[TK_END]的连接去掉
            return 
        else:
            if (inloop==1 and
                    (extra[i+VNODE_PAIR_TYPE] == 'CONTINUE')
                    ):
                # 如果在循环中，且检测到CONTINUE，则需要链接到一开始的迭代节点中
                # 如果在循环中，且检测到break，则需要附加到统一的extra中
                vdraw.conn_node()
            else:
                vdraw.conn_node(extra[i+VNODE_PAIR_NODE], cur, CONN_P2P)

def get_exp_head(exp):
    node = exp[TK_EXPR]
    while isinstance(node, list):
        node = node[0][TK_EXPR]
    return node

# 使用递归的方式来进行显示
def draw_exp(exp, x, y, depth=0, max_depth=100):
    ttype = exp[TK_TYPE]
    if depth >= max_depth:
        # 增加层数
        # 如果层数到达，则只绘制简单node，取第一个接点的注释
        count = 0
        ret = []
        cur_node = -1
        # 如果是最终节点，直接绘制，需要考虑保存本段的入口和出口
        if len(exp[TK_EXPR]) > 0:
            node = exp[TK_EXPR][0]
            node_data = node
            if ENABLE_DRAW_DEBUG==1: print(node)
            cur_node = vdraw.draw_node(NODE_P, x, y+count, get_exp_head(node_data), node[TK_START])
        else:
            cur_node = vdraw.draw_node(NODE_P, x, y+count, 'to be fill', -1)
        # 返回区段的入口、出口
        count += 1
        ret = [0, count, cur_node, cur_node]
        return ret
    elif (
            ttype == 'DONENODE'
            ):
        count = 0
        ret = []
        first_node = -1
        last_node = -1
        # 如果是最终节点，直接绘制，需要考虑保存本段的入口和出口
        for node in (exp[TK_EXPR]):
            if ENABLE_DRAW_DEBUG==1: print(node)
            # 在绘制的时候，需要考虑
            cur_node = vdraw.draw_node(NODE_P, x, y+count, node[TK_EXPR], node[TK_START])
            # 如果是第一个节点，需要将其保存
            if count == 0:
                first_node = cur_node
                last_node = cur_node
            else:
                # 其他的节点需要连线
                vdraw.conn_node(last_node, cur_node)
            last_node = cur_node
            count += 1
        ret = [0, count, first_node, last_node]
        # 返回区段的入口、出口
        return ret
    elif ( ttype == 'DONEBREAK'):
        ret = []
        count = 0
        # 需要判断是否为一个迭代结构的里面
        node = exp[TK_EXPR][0]
        cur_node = vdraw.draw_node(NODE_P, x, y+count, node[TK_EXPR], node[TK_START])
        # 作为break来说，只是将其返回
        ret = [0, 1, cur_node, cur_node, 'BREAK', cur_node]
        return ret

    elif ( ttype == 'DONECONTINUE'
            or ttype == 'DONEGOTO'
            ):
        ret = []
        count = 0
        node = exp[TK_EXPR][0]
        cur_node = vdraw.draw_node(NODE_P, x, y+count, node[TK_EXPR], node[TK_START])
        ret = [0, 1, cur_node, cur_node]
        return ret
    elif (ttype == 'DONERETURN'):
        ret = []
        count = 0
        node = exp[TK_EXPR][0]
        cur_node = vdraw.draw_node(NODE_END, x, y+count, node[TK_EXPR], node[TK_START])
        ret = [0, 1, cur_node, -1]
        return ret
    elif (ttype == 'DONEIFELSE'):
        # IF/ELSE结构不需要关注入口，但是需要关注出口
        #    (可能会有需要继承的接口)
        node = exp[TK_EXPR][0]

        vnode = vdraw.draw_node(NODE_IF, x, y, node[TK_EXPR], node[TK_START])
        # 获取IF函数的节点
        ret1 = draw_exp(exp[TK_EXPR][1], x, y+1, depth+1, max_depth)
        # 获取ELSE函数的节点
        ret2 = draw_exp(exp[TK_EXPR][2], x+1+ret1[VNODE_WIDTH], y, depth+1, max_depth)

        vdraw.conn_node(vnode, ret1[VNODE_ENTRY], CONN_P2P, STR_YES)
        vdraw.conn_node(vnode, ret2[VNODE_ENTRY], CONN_D2P, STR_NO)

        # 在计算else分支的宽度时，需要判断该分支是否空语句，
        #    如果是，取1
        #     如果不是，取0
        if (ret2[VNODE_HEIGHT] != 0):
            width2 = ret2[VNODE_WIDTH] + 1
        else:
            width2 = 0

        # 
        height = max(ret1[VNODE_HEIGHT], ret2[VNODE_HEIGHT]-1)
        width = max(ret1[VNODE_WIDTH], width2)

        # 构建初步的列表
        ret = [width, height+1, vnode, ret1[VNODE_EXIT], 'NODE', ret2[VNODE_EXIT]]
        ret.extend(get_extra_node(ret1))
        ret.extend(get_extra_node(ret2))
        if ENABLE_VISIOD_DEBUG==1: print 'IFELSE', ret
        return ret

    elif (ttype == 'DONEWHILE'):
        # 表达式
        node = exp[TK_EXPR][0]
        vnode = vdraw.draw_node(NODE_IF, x, y, node[TK_EXPR], node[TK_START])
        # 绘制节点
        ret1 = draw_exp(exp[TK_EXPR][1], x, y+1, depth+1, max_depth)
        vdraw.conn_node(vnode, ret1[VNODE_ENTRY], CONN_P2P, STR_YES)

        # 绘制反馈
        vdraw.conn_node(ret1[VNODE_EXIT], vnode, CONN_P2D)
        
        # 构建主要的入口和出口
        ret = [ret1[VNODE_WIDTH], ret1[VNODE_HEIGHT] + 1 , vnode, ret1[VNODE_EXIT], ttype[4:len(ttype)], vnode]
        ret.extend(get_extra_node(ret1))
        return ret

    elif (ttype == 'DONEDEFAULT'):
        node = exp[TK_EXPR][0]
        vnode = vdraw.draw_node(NODE_P, x, y, node[TK_EXPR], node[TK_START])
        # 绘制节点
        ret1 = draw_exp(exp[TK_EXPR][1], x, y+1, depth+1, max_depth)
        vdraw.conn_node(vnode, ret1[VNODE_ENTRY], CONN_P2P, STR_YES)
        
        # 构建主要的入口和出口
        ret = [ret1[VNODE_WIDTH], ret1[VNODE_HEIGHT] + 1 , vnode, ret1[VNODE_EXIT], ttype[4:len(ttype)], vnode]
        extra = get_extra_node(ret1)
        ret.extend(extra)
        if ENABLE_VISIOD_DEBUG==1: print '====', ttype, extra, ret
        return ret
        
    elif (ttype == 'DONEIF'
            or ttype == 'DONECASE'
            ):
        # 表达式
        node = exp[TK_EXPR][0]
        vnode = vdraw.draw_node(NODE_IF, x, y, node[TK_EXPR], node[TK_START])
        # 绘制节点
        ret1 = draw_exp(exp[TK_EXPR][1], x, y+1, depth+1, max_depth)
        vdraw.conn_node(vnode, ret1[VNODE_ENTRY], CONN_P2P, STR_YES)
        
        # 构建主要的入口和出口
        ret = [ret1[VNODE_WIDTH], ret1[VNODE_HEIGHT] + 1 , vnode, ret1[VNODE_EXIT], ttype[4:len(ttype)], vnode]
        ret.extend(get_extra_node(ret1))
        if ENABLE_VISIOD_DEBUG==1: print '====', ttype, ret
        return ret

    elif (ttype == 'DONEDO'):
        # 获取if的第一个节点
        node = exp[TK_EXPR][0]
        # 绘制函数体
        ret1 = draw_exp(exp[TK_EXPR][1], x, y, depth+1, max_depth)
        # 绘制 while
        if ENABLE_DRAW_DEBUG==1: print('=DO', node, ret1)
        vnode = vdraw.draw_node(NODE_IF, x, y+ret1[VNODE_HEIGHT], node[TK_EXPR], node[TK_START])

        vdraw.conn_node(ret1[VNODE_EXIT], vnode, CONN_P2P)

        extra = get_extra_node(ret1)
        conn_extra_node(extra, vnode)

        # while循环需要链接到do
        vdraw.conn_node(vnode, ret1[VNODE_ENTRY], CONN_D2P, STR_YES)
        
        ret = [ret1[VNODE_WIDTH], ret1[VNODE_HEIGHT]+1, ret1[VNODE_ENTRY], vnode]
        ret.extend(get_extra_node(ret1))
        # 考虑到break，需要做一部分的处理
        # to be complete
        return ret
    elif (ttype == 'DONEFOR'):
        # 获取 第一个节点
        node = exp[TK_EXPR][0]

        # 绘制初始化函数和判断函数
        vnode0 = vdraw.draw_node(NODE_P, x, y, node[TK_EXPR0], node[TK_START])
        # 判断节点
        vnode = vdraw.draw_node(NODE_IF, x, y+1, node[TK_EXPR], node[TK_START])
        vdraw.conn_node(vnode0, vnode, CONN_P2P)

        # 绘制函数
        ret1 = draw_exp(exp[TK_EXPR][1], x, y+2, depth+1, max_depth)
        vdraw.conn_node(vnode, ret1[VNODE_ENTRY], CONN_P2P, STR_YES)

        # 绘制迭代函数
        vnode2 = vdraw.draw_node(NODE_P, x, y+2+ret1[VNODE_HEIGHT], node[TK_EXPR2], node[TK_START])
        vdraw.conn_node(ret1[VNODE_EXIT], vnode2, CONN_P2P)
        extra = get_extra_node(ret1)
        conn_extra_node(extra, vnode2)

        # 绘制反馈
        vdraw.conn_node(vnode2, vnode, CONN_R2R)

        ret = [ret1[VNODE_WIDTH], ret1[VNODE_HEIGHT]+3, vnode0, vnode2]
        # 将
        ret.extend(['IF', vnode])

        return ret
        
    elif (ttype == 'DONESWITCH'):
        # 将switch接点作为一个初始的工作
        node = exp[TK_EXPR][0]
        vnode = vdraw.draw_node(NODE_P, x, y, node[TK_EXPR], node[TK_START])

        w = 0
        h = 0
        last_ret=[]
        extra=[]
        last_width = 0
        last_height = 0
        last_token = 'DONEBREAK'    # 用于判断break连线
        # 遍历相关节点
        for node2 in (exp[TK_EXPR][1][TK_EXPR]):
            # 遍历switch里面的分支判断
            if (node2[TK_TYPE] == 'DONECASE'
                    or node2[TK_TYPE] == 'DONEDEFAULT'
                    ):
                # 只在遇到DONECASE
                w += last_width
                last_ret.append(draw_exp(node2, x+w, y+1, depth+1, max_depth))
                if ENABLE_DRAW_DEBUG==1: print 'last', w, last_ret, '\n', node2

                # 如果不是第一个case，需要和之前的case相连
                if w!=0:
                    vdraw.conn_node(last_ret[-2][VNODE_ENTRY], last_ret[-1][VNODE_ENTRY], CONN_D2P, STR_NO)
                last_width = last_ret[-1][VNODE_WIDTH] + 1
                # 需要更新最高数值
                last_height = last_ret[-1][VNODE_HEIGHT] + 1
                if (last_height > h): h = last_height

                # 如果上一次不是BREAK, 需要将case的最后一个语句连到当前case
                if (last_token != 'DONEBREAK'):
                    vdraw.conn_node(last_ret[-2][VNODE_EXIT], last_ret[-1][VNODE_ENTRY], CONN_P2P)
            elif (node2[TK_TYPE] == 'DONEBREAK'):
                # 除了绘制以外，还需要连接
                ret2 = draw_exp(node2, x+w, y+1 + last_ret[-1][VNODE_HEIGHT], depth+1, max_depth)
                if ENABLE_DRAW_DEBUG==1: print 'ret2', ret2

                # 将之前的表达式链接到break上
                vdraw.conn_node(last_ret[-1][VNODE_EXIT], ret2[VNODE_EXIT], CONN_P2P)
                # 将BREAK作为后续的保障
                extra.extend(['BREAK', ret2[VNODE_EXIT]])
    
                # 需要更新最高数值
                last_height += 1
                if (last_height > h): h = last_height
            last_token = node2[TK_TYPE]
        # 将switch和第一个case相连
        vdraw.conn_node(vnode, last_ret[0][VNODE_ENTRY], CONN_P2P)

        ret = [w, h, vnode, last_ret[-1][VNODE_EXIT]]

        # 对于SWITCH而言， 并不存在正规的出口，第一个不包含BREAK语句的CASE就是系统出口
        if (len(extra)>0):
            ret[VNODE_EXIT] = -1
        #    ret[VNODE_EXIT] = extra[VNODE_PAIR_NODE]
        ret.extend(extra)
        if ENABLE_DRAW_DEBUG==1: print 'extra h', h, extra
        return ret

    elif (ttype == 'NODE'):
        # 说明需要解析
        count = 0
        width = 0
        ret = []
        extra = []
        first_node = -1
        last_node = -1
        # 如果是最终节点，直接绘制，需要考虑保存本段的入口和出口
        for node in (exp[TK_EXPR]):
            # 在绘制的时候，需要考虑
            cur_node = draw_exp(node, x, y+count, depth+1, max_depth)
            # 如果是第一个节点，需要将其保存
            if count == 0:
                first_node = cur_node
                last_node = cur_node
            else:
                # 其他的节点需要连线
                vdraw.conn_node(last_node[VNODE_EXIT], cur_node[VNODE_ENTRY])
                extra = get_extra_node(last_node)
                if ENABLE_VISIOD_DEBUG==1: print('====NODE ex', extra)

                for i in range(0,len(extra),2):
                    if (isinstance(extra[i+VNODE_PAIR_NODE], int)):
                        # 如果连接的时候，某个节点node为整数（-1）
                        # 此时需要将其入口链接到当前接点
                        # 此时认为只有判断表达式才会出现这种问题
                        if ENABLE_VISIOD_DEBUG==1: print('====NODE-1', node, last_node)
                        vdraw.conn_node(last_node[VNODE_ENTRY], cur_node[VNODE_ENTRY], CONN_D2P, STR_NO)
                        #if (last_node[VNODE_TYPE] == NODE_IF):
                        #else:
                        #    vdraw.conn_node(last_node[VNODE_ENTRY], cur_node[VNODE_ENTRY], CONN_P2P)

                conn_extra_node(extra, cur_node[VNODE_ENTRY])

            last_node = cur_node
            count += cur_node[VNODE_HEIGHT]
            width += cur_node[VNODE_WIDTH]
            #print(count, exp[TK_START], cur_node)
        # 
        # 返回区段的入口、出口
        ret = [width, count, first_node[VNODE_ENTRY], last_node[VNODE_EXIT]]

        # 将最后一个节点未关闭的导出
        # 此处导致的一个问题是，如果最后一个
        ret.extend(get_extra_node(last_node))

        if ENABLE_VISIOD_DEBUG==1: print('====NODE ret', ret)
        return ret

def draw_fun(name, exp, x, y, max_depth=100, visio=vdraw):
    # 新建页面
    visio.new_page(name)
    # 绘制表达式
    ret = draw_exp(exp, x, y, 0, max_depth)
    # 如果不是整数，表示未画结束框图
    if (isinstance(ret[VNODE_EXIT], int) == False):
        rnode = visio.draw_node(NODE_END, x, y+ret[VNODE_HEIGHT], 'End', -1)
        vdraw.conn_node(ret[VNODE_EXIT], rnode)

if __name__ == "__main__":
    print "gogogo"
