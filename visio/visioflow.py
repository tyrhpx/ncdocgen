#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#    提供使用visio进行流程图绘制的相关工作
#

import win32com.client
import os, sys
import re

#FlowchartTemplateName="Basic Flowchart.vst"    
FlowchartTemplateName="BASFLO_M.VST"    
FlowchartStencilName="BASFLO_M.VSS"
FlowchartTemplateName_12="BASFLO_M.VST"
FlowchartStencilName_12="BASFLO_M.VSS"
FlowchartTemplateName_x="BASFLO_M.VSTX"    
FlowchartStencilName_x="BASFLO_M.VSSX"
MasterProcessName="Process"
MasterDecisionName="Decision"
MasterEndName="Terminator"
VS2010MasterEndName="Start/End" # 在visio2010中使用不同的Name，此处需要注意

# 节点的类型
NODE_P       =0
NODE_IF      =1
NODE_END     =2
NODE_NONE    =3

_node_table=[
"NODE ",#P        
"IF   ",#IF        
"RET  ",#RET
"NONE ",#NONE    
        ]


# P 表示procedure
# D 表示decision
# E 表示east
# W 表示west
# N 表示north
# S 表示south
CONN_P2P = 0    # 
CONN_P2D = 1    #
CONN_D2P = 2
CONN_P2E = 3
CONN_E2E = 4
CONN_E2P = 5
CONN_D2U = 6
CONN_P2N = 7
CONN_D2N = 8
CONN_W2W = 9

# 获取偏移量
OFFSET_INDEX    =0
OFFSET_COMMENT    =1
OFFSET_PROP        =2
OFFSET_X        =3
OFFSET_Y        =4
OFFSET_LINK        =5
OFFSET_LINK2    =6
OFFSET_N        =7

MAP_SHAPE=0
MAP_DATA=1

N=0
S=1
W=2
E=3

_node_dir_map = [   #N  S  W  E
    [3, 2, 0, 1],   #NODE_P
    [2, 3, 0, 1],   #NODE_IF
    [1, 0, 2, 3],   #NODE_END
]

ELEM_TYPE=0

#
# 使用visio进行绘图的类
class visiodraw:
    def __init__(self, filename, autoquit=0):
        self.filename=filename
        self.autoquit=autoquit
        self.pgcount = 0
        self.ver = 0
        self.elem_map = {}

    def push_elem(self, id, type, x=0, y=0):
        self.elem_map[id] = [type, x, y]

    def get_cellsrc(self, node, direction):
        try:
            type = self.elem_map[node.ID][ELEM_TYPE]
            # 获取相关的接口
            return node.CellsSRC(7, _node_dir_map[type][direction], 0)
        except:
            return node.CellsU('PinX')
    
    def save(self, autoquit=True):
        print('saving visio %s', self.filename)
        self.visio.ActiveDocument.SaveAs(self.filename)
        self.visio.ActiveDocument.Close()
        if autoquit:
            self.visio.Quit()

    def open(self, name=''):
        if (name!=''):
            self.filename = get_full_name(name)

        print 'Drawing vsd:', self.filename

        self.visio = win32com.client.Dispatch("Visio.Application")
        #self.Visio = win32com.client.Dispatch("Visio.InvisibleApp")
        self.visio.Visible = 1

        self.ver = int(float(self.visio.Version.decode("utf-8")))
        #print "visio version : ", ver, type(ver)
        # ver 11: visio 2003
        # ver 12: visio 2007
        if (self.ver <= 12):
            FlowchartTemplateName = FlowchartTemplateName_12
            FlowchartStencilName = FlowchartStencilName_12
        elif (self.ver >= 14): # visio 2010, 2013
            FlowchartTemplateName = FlowchartTemplateName_x
            FlowchartStencilName = FlowchartStencilName_x
        else:
            FlowchartTemplateName = FlowchartTemplateName_12
            FlowchartStencilName = FlowchartStencilName_12

        self.objtable = {}

        # add a new file using visio template name 
        self.docFlowTemplate = self.visio.Documents.Add(FlowchartTemplateName)

        # Search open documents for our flowchart stencil:
        for doc in self.visio.Documents :
            #print doc.Name, FlowchartStencilName, doc.Name == FlowchartStencilName
            if (doc.Name == FlowchartStencilName):
                docFlowStencil = doc
                break
        #docFlowStencil = self.docFlowTemplate

        # Step 2: get the masters and connect.
        # ------------------------------------------------
        # Get masters for Process and Decision:
        mstProcess = docFlowStencil.Masters.ItemU(MasterProcessName)
        mstDecision = docFlowStencil.Masters.ItemU(MasterDecisionName)
        try:
            mstEnd = docFlowStencil.Masters.ItemU(MasterEndName)
        except:
            mstEnd = docFlowStencil.Masters.ItemU(VS2010MasterEndName)

        # 标准物体
        self.stdobj = [mstProcess, mstDecision, mstEnd, mstDecision, mstDecision]

        # Get the built-in connector object. Note, it's
        # not typed as a master!
        self.conn = self.visio.Application.ConnectorToolDataObject

        self.dx = 1.5
        self.dy = 0.8

        self.pgcount = 1

    def new_page(self, name):

        if (self.pgcount == 0):
            self.open()
        # We'll draw on the first page of the document,
        # which is probably the only page in the document!
        if (self.pgcount == 1):
            self.pg = self.docFlowTemplate.Pages.Item(self.pgcount)
            # 针对visio2013，设置非填充主题, 2014.4
            if self.ver >=14:
                #self.pg.SetThemeVariant(1, 1)  # 红色判断框主题
                #self.pg.SetThemeVariant(0, 0)
                self.pg.SetTheme(0, 0, 0, 0, 0)
        else:
            # 如果是第二次，则需要增加页
            self.pg = self.docFlowTemplate.Pages.Add()

        self.pgcount += 1
        self.pg.Name = name
        self.pgwidth = self.pg.PageSheet.CellsU("PageWidth").ResultIU
        self.pgheight = self.pg.PageSheet.CellsU("PageHeight").ResultIU
        # Get the center, top of the page:
        self.orix = self.pgwidth / 6
        self.oriy = self.pgheight - 1

        self.elem_map.clear()

        return self.pg

    # 绘制
    def draw_node(self, type, x, y, text, line=0):
        """
        @brief 绘制节点
        @type 节点类型
        @x  x轴偏移量
        @y  y轴偏移量
        @text 文字
        @line 线条宽度
        """
        shape = self.pg.Drop(self.stdobj[type], self.orix + self.dx*x, self.oriy - self.dy*y)
        shape.Text = text
        shape.Cells("Prop.Cost").ResultIU = line
        #shape.Cells("Prop.Cost").ResultIU = shape.ID
        shape.Cells('Height').ResultIU = self.dy*5/8
        #print shape.ID

        self.push_elem(shape.ID, type, x, y)
        return shape

    # 连接接点
    #           2                    1
    #     0  Rect | D  1     2  Ellipse  3
    #           3                    0
    #     默认 CellsU("PinX")
    def conn_node(self, node1, node2, type=CONN_P2P, text='', width=0):
        #print('conn', node1, node2)
        if (isinstance(node1, int)
                or isinstance(node2, int) ):
            # 在RETURN等封闭性的节点上，采用-1用于标记node2
            return
        if (node1 is None
                or node2 is None):
            # 在RETURN等封闭性的节点上，采用-1用于标记node2
            return

        con = self.pg.Drop(self.conn, 0, 0)
        # VisSectionIndices visSectionObject=1
        # VisRowIndices VisRowIndices=2
        # VisCellIndices visLineEndArrow=6
        con.CellsSRC(1, 2, 6).FormulaU = "1"

        self.push_elem(con.ID, type, node1.ID, node2.ID)

        #con = self.pg.Drop(self.conn, self.pgwidth, self.pgheight*0.75)
        if (text != ''):
            con.Text = text
        if (type == CONN_P2P):
            # 将起点连接到当前节点
            con.CellsU("BeginX").GlueTo(node1.CellsU("PinX"))
            # 将终点连接到当前节点
            con.CellsU("EndX").GlueTo(node2.CellsU("PinX"))
            #con.CellsU("EndX").GlueTo(node2.CellsSRC(7, 3, 0))
        elif (type == CONN_D2P):
            # 将起点连接到当前节点
            con.CellsU("BeginX").GlueTo(node1.CellsU("Connections.X2"))
            # 将终点连接到当前节点
            con.CellsU("EndX").GlueTo(node2.CellsU("PinX"))
            #con.CellsU("EndX").GlueTo(node2.CellsSRC(7, 2, 0))
        elif (type == CONN_D2U):
            # 将起点连接到当前节点
            con.CellsU("BeginX").GlueTo(node1.CellsU("Connections.X2"))
            # 将终点连接到当前节点
            #con.CellsU("EndX").GlueTo(node2.CellsU("PinX"))
            con.CellsU("EndX").GlueTo(node2.CellsSRC(7, N, 0))
        elif (type == CONN_P2D):
            # 将起点连接到当前节点
            con.CellsU("BeginX").GlueTo(node1.CellsU("PinY"))
            # 将终点连接到当前节点
            con.CellsU("EndX").GlueTo(node2.CellsU("Connections.X1"))
        elif (type == CONN_P2E):
            # 将起点连接到当前节点
            con.CellsU("BeginX").GlueTo(node1.CellsU("PinX"))
            # 将终点连接到当前节点
            con.CellsU("EndX").GlueTo(node2.CellsU("PinY2"))
        elif (type == CONN_E2E):
            # 将起点连接到当前节点
            con.CellsU("BeginX").GlueTo(node1.CellsU("PinY"))
            con.CellsU("EndX").GlueTo(node2.CellsU("PinY"))
        elif (type == CONN_E2P):
            con.CellsU("BeginX").GlueTo(node1.CellsSRC(7, 1, 0))
            con.CellsU("EndX").GlueTo(node2.CellsU("PinX"))
            #con.CellsU("EndX").GlueTo(node2.CellsSRC(7, 0, 0))
        elif (type == CONN_P2N):
            con.CellsU("BeginX").GlueTo(node1.CellsU("PinX"))
            con.CellsU("EndX").GlueTo(self.get_cellsrc(node2, N))
        elif (type == CONN_D2N):
            con.CellsU("BeginX").GlueTo(node1.CellsU("Connections.X2"))
            con.CellsU("EndX").GlueTo(self.get_cellsrc(node2, N))
        elif (type == CONN_W2W):
            con.CellsU("BeginX").GlueTo(self.get_cellsrc(node1, W))
            con.CellsU("EndX").GlueTo(self.get_cellsrc(node2, W))
        else:
            return

        # 更新线段的width
        pass

    def copy2sys(self):
        # 全选页内的所有元素
        self.visio.ActiveWindow.SelectAll()
        # 复制
        self.visio.ActiveWindow.Selection.Copy()

    #
    # 关闭文件
    def close(self):
        # Deselect all shapes, so it looks better:
        self.visio.ActiveWindow.DeselectAll
        
        # Tile the windows so the user doesn't freak out!
        self.visio.ActiveDocument.SaveAs(self.filename)
        # 判断自动关闭
        if (self.autoquit==1):
            self.visio.ActiveDocument.Close()
            self.visio.Quit()
    
    def draw_table(self, table):
        try:
            # 遍历表中的元素
            for elem in table: 
                # 放置图形

                if (elem[OFFSET_PROP] == NODE_NONE):
                    continue

                shape = self.draw_node(elem[OFFSET_PROP], elem[OFFSET_X], elem[OFFSET_Y], elem[OFFSET_COMMENT], elem[OFFSET_INDEX])
                self.objtable[elem[OFFSET_INDEX]] = [shape, elem]

            for elem in self.objtable.itervalues():
                #print elem    # 打印所有的连接
                if (elem[MAP_DATA][OFFSET_LINK] != -1):
                    node=self.objtable[elem[MAP_DATA][OFFSET_LINK]]
                    text = ''
                    if (elem[MAP_DATA][OFFSET_PROP] == NODE_IF):
                        text = 'YES'
                    self.conn_node(elem[MAP_SHAPE], node[MAP_SHAPE], CONN_P2P, text)

                if (len(elem[MAP_DATA]) > OFFSET_LINK2
                        and elem[MAP_DATA][OFFSET_LINK2] != -1
                        ):
                    node=self.objtable[elem[MAP_DATA][OFFSET_LINK2]]
                    self.conn_node(elem[MAP_SHAPE], node[MAP_SHAPE], CONN_D2P, 'NO')
        except:
            return 

    # 绘制表格
    def draw(self, table):
        Visio = win32com.client.Dispatch("Visio.Application")
        #Visio = win32com.client.Dispatch("Visio.InvisibleApp")
        Visio.Visible = 1

        self.ver = int(float(self.visio.Version.decode("utf-8")))
        if (self.ver == 12):
            FlowchartTemplateName = FlowchartTemplateName_12
            FlowchartStencilName = FlowchartStencilName_12
        elif (self.ver >= 14): # visio 2010, 2013
            FlowchartTemplateName = FlowchartTemplateName_x
            FlowchartStencilName = FlowchartStencilName_x

        docFlowTemplate = Visio.Documents.Add(FlowchartTemplateName)

        # Search open documents for our flowchart stencil:
        for doc in Visio.Documents :
            if (doc.Name == FlowchartStencilName):
                docFlowStencil = doc

        # Step 2: get the masters and connect.
        # ------------------------------------------------
        # Get masters for Process and Decision:
        mstProcess = docFlowStencil.Masters.ItemU(MasterProcessName)
        mstDecision = docFlowStencil.Masters.ItemU(MasterDecisionName)

        # 标准物体
        stdobj = [mstProcess, mstProcess, mstDecision, mstDecision, mstDecision, mstDecision]

        # Get the built-in connector object. Note, it's
        # not typed as a master!
        conn = Visio.Application.ConnectorToolDataObject

        dx = 1.5
        dy = 1

        # We'll draw on the first page of the document,
        # which is probably the only page in the document!
        pg = docFlowTemplate.Pages.Item(1)

        # Get the center, top of the page:
        orix = pg.PageSheet.CellsU("PageWidth").ResultIU / 2
        oriy = pg.PageSheet.CellsU("PageHeight").ResultIU - 1

        # 新的物体
        objtable = {}
        objnum = 0

        # 
        try:
            # 遍历表中的元素
            for elem in table: 
                # 放置图形

                if (elem[OFFSET_PROP] == NODE_NONE):
                    continue
                shape = pg.Drop(stdobj[elem[OFFSET_PROP]], orix + dx*elem[OFFSET_X], oriy + dy*elem[OFFSET_Y])
                #node_text = elem[OFFSET_COMMENT].split('\n')[0]
                #node_name = re.search('[^\*<]+.*?', node_text)
                #shape.Text = node_name.group(0)
                shape.Text = elem[OFFSET_COMMENT]
                #shape.Cells("Prop.Cost").ResultIU = i
                objtable[elem[OFFSET_INDEX]] = [shape, elem]

            for elem in objtable.itervalues():
                #print elem    # 打印所有的连接
                if (elem[MAP_DATA][OFFSET_LINK] != -1):
                    # 放置连接
                    con = pg.Drop(conn, 0, 0)
                    node=objtable[elem[MAP_DATA][OFFSET_LINK]]
                    # 将起点连接到当前节点
                    con.CellsU("BeginX").GlueTo(elem[MAP_SHAPE].CellsU("PinX"))
                    # 将终点连接到当前节点
                    con.CellsU("EndX").GlueTo(node[MAP_SHAPE].CellsU("PinX"))
                    if (elem[MAP_DATA][OFFSET_PROP] == NODE_IF):
                        con.Text = "Yes"

                #print len(elem[MAP_DATA])
                if (len(elem[MAP_DATA]) > OFFSET_LINK2 and elem[MAP_DATA][OFFSET_LINK2] != -1):
                    con = pg.Drop(conn, 0, 0)
                    node=objtable[elem[MAP_DATA][OFFSET_LINK2]]
                    # 将起点连接到当前节点
                    con.CellsU("BeginX").GlueTo(elem[MAP_SHAPE].CellsU("Connections.X2"))
                    # 将终点连接到当前节点
                    con.CellsU("EndX").GlueTo(node[MAP_SHAPE].CellsU("PinX"))
                    if (elem[MAP_DATA][OFFSET_PROP] == NODE_IF):
                        con.Text = "No"

            #print objtable

            # Step 4: Layout the shapes and deselect.
            # ------------------------------------------------
            # Deselect all shapes, so it looks better:
            Visio.ActiveWindow.DeselectAll
            
            # Tile the windows so the user doesn't freak out!
            Visio.ActiveDocument.SaveAs(self.filename)
            # 判断自动关闭
            if (self.autoquit==1):
                Visio.ActiveDocument.Close()
                Visio.Quit()

        except Exception, e:
            print "Error", e

# 获取完整文件名
def get_full_name(name):
    if (len(name.split(':')) == 1):
        filename = os.path.dirname(sys.argv[0])+"\\"+name
    else:
        filename = name
    return filename

def get_node_name(n):
    return _node_table[n]

#OFFSET_INDEX=0 OFFSET_COMMENT=1 OFFSET_PROP=2 OFFSET_X=3 OFFSET_Y=4 OFFSET_LINK=5 OFFSET_LINK2=6
test_sample=[
        [0, u"开始一个十分长的函数功能模块", NODE_P, 0, 0, 1],
        [1, u"干活"  , NODE_P, 0, 1, 2],
        [2, u"判断"  , NODE_IF,0, 2, 3, 4],
        [3, u"OK"    , NODE_P, 0, 3, 5],
        [4, u"Not OK", NODE_P, 1, 3, 5],
        [5, u"清理"  , NODE_P, 0, 4, -1],
        ]

def print_chart_data(chart):
    for node in chart:
        print(node[OFFSET_INDEX], node[OFFSET_X:OFFSET_LINK2+1],
                get_node_name(node[OFFSET_PROP]), node[OFFSET_COMMENT], 
                )

if __name__ == "__main__":
    name = "sample2.vsd"
    if (len(sys.argv) > 1):
        name = sys.argv[1]

    filename = get_full_name(name)
    print "Testing sample", filename

    obj = visiodraw(filename)
    #obj.draw(test_sample)
    obj.open()
    obj.new_page('Haha')

    obj.new_page('Haha2')
    obj.draw_table(test_sample)
