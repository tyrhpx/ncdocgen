rd /s /q dist
py -2 -c "import cdocgen; print cdocgen.main_version.split('-')[0]," > mainver.txt
py -2 setup.py py2exe
mkdir nsisbuild || del /q nsisbuild\*
cd nsisbuild && cmake -G"MinGW Makefiles" .. && cmake --build . && cpack
cd ..
