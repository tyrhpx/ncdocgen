/** 
 * @brief make_argv_r 制造参数列表
 * 
 * @param buff 输入的字符串缓冲
 * @param argv 目标参数列表
 * 
 * @return 参数数量
 */
uint16_t make_argv_r(uint8_t *buff, uint8_t *argv[])
{
	int16_t i, in_text;

	/* break buff int8_to strings and argv */
	/* it is permissible for argv to be NULL, in which case */
	/* the purpose of this routine becomes to count args */
	uint16_t argc = 0;
	i = 0;
	/*- 初始化 */
	in_text = 0;
	/*- 当前字符不为\0 */
	while (buff[i] != '\0')	/* getline() must place 0x00 on end */
	{
		/*- 如果字符为' ','\t','\r' */
		if (((buff[i] == ' ')   ||
					(buff[i] == '\t') || (buff[i] == '\r')) )
		{
			/*- 已识别一个单词 */
			if (in_text)
			{
				/* 填充\0 */
				buff[i] = '\0';
				in_text = 0;
			}
			else
			{
				/* still looking for next argument */
			}
		}
		else
		{
			/* got non-whitespace character */
			if (in_text)
			{
			}
			else
			{
				/* start of an argument */
				in_text = 1;
				/*- 如果当前argv数组未满 */
				if (argc < CONFIG_MAX_ARGC)
				{
					/*- 更新argv列表 */
					argv[argc] = (char*)&buff[i];
					argc++;
				}
				else
					/*return argc;*/
					break;
			}

		}
		/* proceed to next character */
		/*- 处理下个字符*/
		i++;	
	}

	/*如果没有达到CONFIG_MAX_ARGC,则将char * argv[]数组剩余元素全部赋值为NULL */
	/*- @alias 清除参数列表的尾部 */
	for (i=argc;i<CONFIG_MAX_ARGC-1;i++)
	{
		argv[argc] = NULL;
	}
	return argc;
}

/** 
 * @brief be_set_uint32_t 将uint32按照big-endian转化为byte数组
 * 
 * @param dst 待存入的数组
 * @param t 数据
 */
static void * be_set_uint32(uint8_t *dst, uint32_t t)
{
	dst[0] = (t>>24)&0x000000ff;
	dst[1] = (t>>16)&0x000000ff;
	dst[2] = (t>> 8)&0x000000ff;
	dst[3] = (t>> 0)&0x000000ff;
}
