int16_t fsm_update(fsm_t *mode, const uint16_t msg, const fsm_dscrp_t *desp)
{
	uint16_t i=0U, j=0U;
	int16_t ret;
	/*- 描述表有效 */
	if (desp)
	{
		/*- 定位至表头;未到达表尾;轮询描述表 */
		for (i=0U; i<desp->len; i++)
		{
			/*- 模式匹配当前描述表 */
			if (*mode == desp->table[i].cur_mode)
			{
				/*- 更新状态机 */
				ret = fsm_convert(msg, &desp->table[i]);
				/*- @alias 更新模式和返回值 */
				if (ret >=0 )
				{
					*mode = (fsm_t)ret;
					break;
				}else{
					return ret;
				}
			}
		}
	}
	else
	{
		/*- 返回非法指针 */
		return -E_NULL_PT;
	}

	/*- 无错误 */
	return 0;
}

