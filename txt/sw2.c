
/**
* @brief f1 进行switch的简单测试
* @param a 为输入的一个参数
* @detail 包含主要的break分支，包含一个不包含break的分支
*/
void f1(int a)
{
	if (a){
		/** sw */
		switch(e1)
		{
			case e2:
				break;

			case e3:
				/** go */
				go();	/**< sdf */
				break;

			case e4:
				gogo();
				break;

			case e42:
				gogo2();
				gogo3();
				break;

				/*
				   case e32:
				   go();

				   case e4:
				   go();
				   return ;

*/
				//			default:
				//				/** sdf */
				//				sdf();
				//				break;
			default: break;
		}
	}
	asgg();
	switch (step)
	{
		case 1U:
			/** 初始化操作 */
			ret = fnLogInit((int)1U);
			break;
		case 2U:
			/** 输入 */
			ret = fnLogPostRecv();
			break;
		case 3U:
			/** 输出 */
			ret = fnLogSend();
			break;
		case 4U:
			/** 周期初始化操作 */
			ret = fnLogInit((int)2U);
			break;
		case 6U: 
			tt();
			break;
		default: break;
	}

}
