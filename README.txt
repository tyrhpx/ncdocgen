修订记录
============

cdocgen - A C-file documentation generator

cdocgen是一个功能强大的自文档化工具，可以根据doxygen风格的注释，
生成visio格式的流程图，并可以自动插入到详细设计中。

v0.3.9
-------------
2018.5.3 kaikuo.zhuo@hotmail.com
修订记录
 - 在流程图中增加单向箭头

v0.3.7-v0.3.8
-------------
2017.6 kaikuo.zhuo@hotmail.com
修订记录
 - 修复相关bug

releasev0.3.6
-------------
2017.6.29 kaikuo.zhuo@hotmail.com
修订记录
 - 增加全局变量模式

releasev0.3.5
-------------
2017-4 kaikuo.zhuo@hotmail.com
修订记录
 - 增加了md模式

releaseV0.3.2
-------------
2014-4-15 kaikuo.zhuo@hotmail.com
修订记录
 - 修复了user目录的相对路径问题
 - 针对word2013，增加了调用后阅读模式的检查功能（阅读模式下无法操作）

releaseV0.3.1
-------------
2014-4-15 kaikuo.zhuo@hotmail.com
修订记录
 - 修复了全局变量输入时，字符串解析的问题
 - visio改用无主题作为默认主题

releaseV0.3.0
-------------
2014-4-13 kaikuo.zhuo@hotmail.com
修订记录
 - 初步修正了全局变量在存在结构体时的识别bug
 - 为了保证加密后的兼容性，额外发布cdocgen.pyc文件

releaseV0.2.5
-------------
2014-4-13 kaikuo.zhuo@hotmail.com
修订记录
 - 强化了全局变量在存在结构体时的识别bug
 - 增加了cpack方法制作压缩包


releaseV0.2.4
-------------
2014-4-10 kaikuo.zhuo@hotmail.com
修订记录
 - 补充了被修改的全局变量的自动填充功能
 - 修改visio2013默认主题采用白色字体的行为

releaseV0.2.3
-------------
2013-12-25 kaikuo.zhuo@hotmail.com
修订记录
 - 针对不同版本的visio进行了适配

releaseV0.2.2
-------------
2012-7-22 kaikuo.zhuo@hotmail.com
修订记录
 - 针对case丢失语句的事情，进行一定的处理

releaseV0.2b
-------------
2012-2-12 kaikuo.zhuo@hotmail.com
修订记录
 - 增加``作为标识符的扩充标志，允许使用``之间的任何字符作为标识符，从而支持更加方便的注释语法，是的直接画图成为可能
 - 智能去除末尾的;

releaseV0.2a
-------------
2012-2-4 kaikuo.zhuo@hotmail.com
修订记录
 - 增加/*- */风格注释，和doxygen分开，允许和doxygen提取不同的内容形成不同文件，推荐函数头部和doxygen一致，函数体内注释使用/*- */风格
 - 默认支持宽松的语法规则，-e模式用于检查
 - 增强了switch/case语句的解析

releaseV0.2
-------------
2012-2-2 kaikuo.zhuo@hotmail.com
修订记录
 - 增加--keyword选项支持自定义关键字列表功能，允许自定义关键字
 - 补充_T后缀作为类型的扩展
 - 重新确认了case和default逻辑，可以较好处理case/default后空语句
 - 增加sextra_push，SINGLE节点定义，简化单语句处理
 - 兼容doxygen风格，修改详细设计关键字为@details
 - 扩展了增强模式，支持处理if/else中的非复合语句
 - 修改--silent为--verbose模式
 - 增加了文件名的通配符支持
 - 更新了帮助文件

releaseV0.1a
-------------
2012-2-1 kaikuo.zhuo@hotmail.com
修订记录
 - 首次发布

****************************************************************
