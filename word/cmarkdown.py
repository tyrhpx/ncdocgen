#!/usr/bin/python
# -*- coding: utf-8 -*-

import copy
import codecs
import logging

md_dd_head=u"# 详细设计"

md_dd_tpl=u"""
## 子函数%(name)s

### 函数说明表

Table: 函数%(name)s描述表

| 条    目 | 内容 | 
|-----|-----|
| 原    型 | %(proto)s | 
| 概    述 | %(brief)s | 
| 参    数 | %(param)s | 
| 返 回 值 | %(return)s | 
| 引    用 | %(fcall)s | 
| 全局变量<br>引用 | %(gvarin)s | 
| 全局变量<br>修改 | %(gvarout)s | 
| 说    明 | %(details)s | 

### 程序流程

![图 函数%(name)s流程图](images/%(name)s.png)

"""

class mddoc:
    """
    markdown doc class
    """
    def __init__(self, filename, add_title=False):
        """
         @brief 初始化函数
         @param filename 文件名
         @param add_title 是否增加标题
        """
        self.fname = filename
        self.texts = []
        if add_title:
            self.texts.append(md_dd_head)
    
    def insert_fun(self, title, finfo=None):
        """
         @brief 插入函数
         @param title 标题
         @param finfo 函数相关信息
        """
        dinfo = {}
        a=""
        for k,v in finfo.items():
            dinfo[k] = v.strip().replace('\r', '<br>').replace('\n', '<br>')
        dinfo['name'] = title
        raw_text = md_dd_tpl%dinfo
        self.texts.append(raw_text)

    def close(self):
        """
         @brief 关闭
        """
        logging.warn('writing md %s', self.fname)
        with codecs.open(self.fname, 'w', 'utf-8') as f:
            f.writelines(self.texts)
